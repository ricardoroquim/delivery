@extends('app')

@section('content')
<div class="container">
	<h1>Editar cliente: {{$client->user->name}}</h1>

	@include('errors._error')

	{!! Form::model($client, ['route'=>['admin.client.update', $client->id]]) !!}

	@include('admin.clients._form')

	<div class="form-group">
		{!! Form::submit('Salvar', ['class'=>'btn btn-success']) !!}
	</div>

	{!! Form::close() !!}

</div>

@endsection