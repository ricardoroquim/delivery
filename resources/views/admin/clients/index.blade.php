@extends('app')

@section('content')
<div class="container">
	<h1>Clientes</h1>
	<h2>
		<a href="{{ route('admin.client.create') }}" class="btn btn-default">Novo</a>
	</h2>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>ID</th>
				<th>Nome</th>
				<th>E-mail</th>
				<th>Telefone</th>
				<th>Endereço</th>
				<th>Cidade</th>
				<th>Estado</th>
				<th>CEP</th>
				<th>Ações</th>
			</tr>
		</thead>
		<tbody>
		@foreach($clients as $client)
			<tr>
				<td>{{ $client->id }}</td>
				<td>{{ $client->user->name }}</td>
				<td>{{ $client->user->email }}</td>
				<td>{{ $client->phone }}</td>
				<td>{{ $client->address }}</td>
				<td>{{ $client->city }}</td>
				<td>{{ $client->state }}</td>
				<td>{{ $client->zipcode }}</td>
				<td>
					<a href="{{route('admin.client.edit',['id'=>$client->id])}}" class="btn btn-default btn-sm">Editar</a>
					<a href="{{route('admin.client.destroy',['id'=>$client->id])}}" class="btn btn-danger btn-sm">Excluir</a>
				</td>
			</tr>
		</tbody>
		@endforeach 
	</table>
	{!! $clients->render() !!}
</div>

@endsection