@extends('app')

@section('content')
<div class="container">
	<h1>Criar cliente</h1>

	@include('errors._error')

	{!! Form::open(['route'=>'admin.client.store', 'class'=>'form']) !!}

	@include('admin.clients._form')

	<div class="form-group">
		{!! Form::submit('Criar', ['class'=>'btn btn-success']) !!}
	</div>

	{!! Form::close() !!}

</div>

@endsection