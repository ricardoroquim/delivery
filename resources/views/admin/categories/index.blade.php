@extends('app')

@section('content')
<div class="container">
	<h1>Categorias</h1>
	<h2>
		<a href="{{ route('admin.category.create') }}" class="btn btn-default">Criar categoria</a>
	</h2>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>ID</th>
				<th>Nome</th>
				<th>Ação</th>
			</tr>
		</thead>
		<tbody>
		@foreach($categories as $category)
			<tr>
				<td>{{ $category->id }}</td>
				<td>{{ $category->name }}</td>
				<td>
					<a href="{{route('admin.category.edit',['id'=>$category->id])}}" class="btn btn-default btn-sm">Editar</a>
				</td>
			</tr>
		</tbody>
		@endforeach 
	</table>
	{!! $categories->render() !!}
</div>

@endsection