@extends('app')

@section('content')
<div class="container">
	<h1>Editar categoria: {{$category->name}}</h1>

	@include('errors._error');

	{!! Form::model($category, ['route'=>['admin.category.update', $category->id]]) !!}

	@include('admin.categories._form');

	<div class="form-group">
		{!! Form::submit('Salvar categoria', ['class'=>'btn btn-success']) !!}
	</div>

	{!! Form::close() !!}

</div>

@endsection