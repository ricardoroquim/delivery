@extends('app')

@section('content')
<div class="container">
	<h1>Criar categoria</h1>

	@include('errors._error');

	{!! Form::open(['route'=>'admin.category.store', 'class'=>'form']) !!}

	@include('admin.categories._form');

	<div class="form-group">
		{!! Form::submit('Criar categoria', ['class'=>'btn btn-success']) !!}
	</div>

	{!! Form::close() !!}

</div>

@endsection