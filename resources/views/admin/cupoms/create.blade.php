@extends('app')

@section('content')
<div class="container">
	<h1>Criar cupom</h1>

	@include('errors._error')

	{!! Form::open(['route'=>'admin.cupom.store', 'class'=>'form']) !!}

	@include('admin.cupoms._form')

	<div class="form-group">
		{!! Form::submit('Criar cupom', ['class'=>'btn btn-success']) !!}
	</div>

	{!! Form::close() !!}

</div>

@endsection