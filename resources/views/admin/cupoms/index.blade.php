@extends('app')

@section('content')
<div class="container">
	<h1>Cupons</h1>
	<h2>
		<a href="{{ route('admin.cupom.create') }}" class="btn btn-default">Criar cupom</a>
	</h2>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Código</th>
				<th>Valor</th>
				<th>Ação</th>
			</tr>
		</thead>
		<tbody>
		@foreach($cupoms as $cupom)
			<tr>
				<td>{{ $cupom->code }}</td>
				<td>R$ {{ $cupom->value }}</td>
				<td>
					--
				</td>
			</tr>
		</tbody>
		@endforeach 
	</table>
	{!! $cupoms->render() !!}
</div>

@endsection