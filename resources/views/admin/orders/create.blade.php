@extends('app')

@section('content')
<div class="container">
	<h1>Fazer pedido</h1>

	@include('errors._error');

	{!! Form::open(['route'=>'admin.category.store', 'class'=>'form']) !!}

	@include('admin.orders._form');

	<div class="form-group">
		{!! Form::submit('Fazer pedido', ['class'=>'btn btn-success']) !!}
	</div>

	{!! Form::close() !!}

</div>

@endsection