@extends('app')

@section('content')
<div class="container">
	<h1>Pedido {{$order->id}}</h1>
	<ul class="list-unstyled">
		<li><b>Total:</b> R$ {{$order->total}}</li>
		<li><b>Data:</b> {{$order->created_at}}</li>
		<li><b>Cliente:</b> {{$order->client->user->name}}</li>
		<li><b>Endereço de entrega:</b> {{$order->client->address}} - {{$order->client->city}} - {{$order->client->state}}</li>
	</ul>

	@include('errors._error')

	{!! Form::model($order, ['route'=>['admin.order.update', $order->id]]) !!}

	@include('admin.orders._form')

	<div class="form-group">
		{!! Form::submit('Salvar', ['class'=>'btn btn-success']) !!}
	</div>

	{!! Form::close() !!}

</div>

@endsection