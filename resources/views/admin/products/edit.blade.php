@extends('app')

@section('content')
<div class="container">
	<h1>Editar produto: {{$product->name}}</h1>

	@include('errors._error')

	{!! Form::model($product, ['route'=>['admin.product.update', $product->id]]) !!}

	@include('admin.products._form')

	<div class="form-group">
		{!! Form::submit('Salvar', ['class'=>'btn btn-success']) !!}
	</div>

	{!! Form::close() !!}

</div>

@endsection