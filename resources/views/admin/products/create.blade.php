@extends('app')

@section('content')
<div class="container">
	<h1>Criar produto</h1>

	@include('errors._error');

	{!! Form::open(['route'=>'admin.product.store', 'class'=>'form']) !!}

	@include('admin.products._form');

	<div class="form-group">
		{!! Form::submit('Criar produto', ['class'=>'btn btn-success']) !!}
	</div>

	{!! Form::close() !!}

</div>

@endsection