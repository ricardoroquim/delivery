@extends('app')

@section('content')
<div class="container">
	<h1>Produtos</h1>
	<h2>
		<a href="{{ route('admin.product.create') }}" class="btn btn-default">Criar produto</a>
	</h2>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>ID</th>
				<th>Nome</th>
				<th>Categoria</th>
				<th>Preço</th>
				<th>Ação</th>
			</tr>
		</thead>
		<tbody>
		@foreach($products as $product)
			<tr>
				<td>{{ $product->id }}</td>
				<td>{{ $product->name }}</td>
				<td>{{ $product->category->name }}</td>
				<td>{{ $product->price }}</td>
				<td>
					<a href="{{route('admin.product.edit',['id'=>$product->id])}}" class="btn btn-default btn-sm">Editar</a>
					<a href="{{route('admin.product.destroy',['id'=>$product->id])}}" class="btn btn-danger btn-sm">Excluir</a>
				</td>
			</tr>
		</tbody>
		@endforeach 
	</table>
	{!! $products->render() !!}
</div>

@endsection