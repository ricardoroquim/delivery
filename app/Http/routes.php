<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('welcome');
});

Route::auth();

Route::group(['prefix'=>'customer', 'middleware'=>'auth.checkrole:client', 'as'=>'customer.'], function(){
    Route::get('/order', ["as"=>"order.index", "uses"=>"CheckoutController@index"]);
    Route::get('/order/create', ["as"=>"order.create", "uses"=>"CheckoutController@create"]);
    Route::post('/order/store', ["as"=>"order.store", "uses"=>"CheckoutController@store"]);
});

Route::group(['prefix'=>'admin','middleware'=>'auth.checkrole:admin', 'as'=>'admin.'], function() {
    Route::get('/home', 'HomeController@index');

    Route::group(['prefix'=>'categorias', 'as'=>'category.'], function(){
        Route::get('/', ["as"=>"index", "uses"=>"CategoryController@index"]);
        Route::get('/criar',["as"=>"create", "uses"=>"CategoryController@create"]);
        Route::get('/editar/{id}',["as"=>"edit", "uses"=>"CategoryController@edit"]);
        Route::post('/update/{id}',["as"=>"update", "uses"=>"CategoryController@update"]);
        Route::post('/store',["as"=>"store", "uses"=>"CategoryController@store"]);
    });

    Route::group(['prefix'=>'produtos', 'as'=>'product.'], function(){
        Route::get('/', ["as"=>"index", "uses"=>"ProductController@index"]);
        Route::get('/criar',["as"=>"create", "uses"=>"ProductController@create"]);
        Route::get('/editar/{id}',["as"=>"edit", "uses"=>"ProductController@edit"]);
        Route::post('/update/{id}',["as"=>"update", "uses"=>"ProductController@update"]);
        Route::post('/store',["as"=>"store", "uses"=>"ProductController@store"]);
        Route::get('/destroy/{id}',["as"=>"destroy", "uses"=>"ProductController@destroy"]);
    });

    Route::group(['prefix'=>'clientes', 'as'=>'client.'], function(){
        Route::get('/', ["as"=>"index", "uses"=>"ClientController@index"]);
        Route::get('/criar',["as"=>"create", "uses"=>"ClientController@create"]);
        Route::get('/editar/{id}',["as"=>"edit", "uses"=>"ClientController@edit"]);
        Route::post('/update/{id}',["as"=>"update", "uses"=>"ClientController@update"]);
        Route::post('/store',["as"=>"store", "uses"=>"ClientController@store"]);
        Route::get('/destroy/{id}',["as"=>"destroy", "uses"=>"ClientController@destroy"]);
    });

    Route::group(['prefix'=>'orders', 'as'=>'order.'], function(){
        Route::get('/', ["as"=>"index", "uses"=>"OrderController@index"]);
        Route::get('/criar',["as"=>"create", "uses"=>"OrderController@create"]);
        Route::get('/editar/{id}',["as"=>"edit", "uses"=>"OrderController@edit"]);
        Route::post('/update/{id}',["as"=>"update", "uses"=>"OrderController@update"]);
        Route::post('/store',["as"=>"store", "uses"=>"OrderController@store"]);
        Route::get('/destroy/{id}',["as"=>"destroy", "uses"=>"OrderController@destroy"]);
    });

    Route::group(['prefix'=>'cupoms','as'=>'cupom.'], function(){
        Route::get('/', ["as"=>"index", "uses"=>"CupomController@index"]);
        Route::get('/criar',["as"=>"create", "uses"=>"CupomController@create"]);
        Route::post('/store',["as"=>"store", "uses"=>"CupomController@store"]);
    });
});

