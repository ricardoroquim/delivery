<?php

namespace CodeDelivery\Http\Controllers;

use CodeDelivery\Repositories\UserRepository;
use Illuminate\Http\Request;

use CodeDelivery\Http\Requests;
use CodeDelivery\Http\Requests\AdminOrderRequest;
use CodeDelivery\Repositories\OrderRepository;

class OrderController extends Controller
{

	private $repository;

	public function __construct(OrderRepository $repository)
	{
		$this->repository = $repository;
	}

    public function index()
    {	
    	$orders = $this->repository->paginate(10);

        return view('admin.orders.index', compact('orders'));
    }

    public function edit($id, UserRepository $userRepository)
    {
        $list_status = [0=>'Pendente', 1=>'A caminho', 2=>'Entregue'];
        $order = $this->repository->find($id);
        $deliveryman = $userRepository->getDeliveryMan();

        return view('admin.orders.edit', compact('order','list_status','deliveryman'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $this->repository->update($data, $id);

        return redirect()->route('admin.order.index');
    }
}
